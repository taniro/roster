# Roster


## Basic Usage
```javascript

import Roster from '@taniro/roster'

let roster = new Roster("RosterName");

roster.register("key1", val1);
roster.register("key2", val2);

/// get value for key1
roster.valueForKey("key1");

/// get list for all registered items
console.log(roster.list);

```