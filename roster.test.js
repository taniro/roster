const roster = require('./roster');

var r1, r2;

test('roster creation', () => {

    r1 = new roster("list");
    expect(r1).toBeDefined();

});


test('roster prevent roster with the same name', () => {

    expect(() => {
        new roster("list");
    }).toThrow();

});


test('roster creation 2', () => {

    r2 = new roster("list2");
    expect(r2).toBeDefined();
    expect(r2).not.toBe(r1);

});


let A = { 'a': '"a" object'};
let B = { 'b': '"b" object'};

test('roster API test', () => {

    expect(r1.register('a', A)).toBe(true);
    expect(r1.register('b', B)).toBe(true);

    expect(r1.register('a', ["a"])).toBe(false);

    expect(r1.valueForKey('a')).toBe(A);
    expect(r1.valueForKey('b')).toBe(B);

});


test('roster instance interference check', () => {

    let r2B = ["b"];

    expect(r2.valueForKey('a')).toBeUndefined();

    expect(r2.register('b', r2B)).toBe(true);
    expect(r2.valueForKey('b')).toBe(r2B);

});

test('roster list test', () => {

    expect(r1.list).toEqual([A,B]);

});

