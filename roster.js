
const NamedRosters = {};

function Roster (rostername) {

    if (!(this instanceof Roster)) {
        return new Roster(rostername);
    }

    // Keep track of initialized elements
    var API = {},
        _roster = {},
        _list = []
    ;

    Object.defineProperty(API, "register", {
      get: function() {
        return function(key, entity) {
            if( API.valueForKey(key) === undefined ) {
                _roster[key] = entity;
                _list.push(entity);
                return true;
            } else {
                console.error("[Roster][name=" + rostername + "] error duplicate key: '" + key + "'");
            }
            return false;
        }
      },
      configurable: false,
      enumerable: false
    });

    Object.defineProperty(API, "valueForKey", {
      get: function() {
        return function(key) { return _roster[key]; }
      },
      configurable: false,
      enumerable: false
    });

    Object.defineProperty(API, "list", {
      get: function() {
        return _list;
      },
      configurable: false,
      enumerable: true
    });


    /// ============================================================
    /// ============================================================

    /// Register named roster
    if( rostername && rostername != "" ) {

        if( NamedRosters[rostername] !== undefined ) {
            throw new Error("Roster already exists: " + rostername);
        }

        NamedRosters[rostername] = API;

    }

    return API;

}

module.exports = Roster;